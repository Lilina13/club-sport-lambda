-- Création de la base de données
CREATE DATABASE clubLambda;

-- Utiliser la nouvelle base de donnée 
USE clubLambda;

-- Création d'un utilisateur admin 
CREATE USER 'adminClub'@'%' IDENTIFIED BY 'Gonflette33';
-- et on lui donne tous les droits sur la base de données et sur toutes les tables de la base

GRANT ALL PRIVILEGES ON clubLambda.* TO 'adminClub'@'%';




